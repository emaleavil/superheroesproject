package com.eeema.superheroesproject

import com.eeema.superheroesproject.ApiRestTest.RESPONSECODES.OK
import com.eeema.superheroesproject.datasource.remote.ApiRest
import com.eeema.superheroesproject.datasource.rules.TestSchedulerRule
import com.eeema.superheroesproject.model.Heroe
import com.eeema.superheroesproject.model.SuperHeroes
import com.eeema.superheroesproject.utils.FileExtensions
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.hasSize
import org.hamcrest.core.IsNull.notNullValue
import org.junit.After
import org.junit.Before
import org.junit.Rule
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


/**
 * Created by 0013440 on 21/04/2017.
 */
open class ApiRestTest {

    enum class RESPONSECODES(val code : Int) {
        OK(200),
        NOT_FOUND(404),
        SERVER_DOWN(500)
    }

    lateinit var server : MockWebServer

    @JvmField @Rule val schedulerRule = TestSchedulerRule()

    @Before
    open fun setUp() {
        server = MockWebServer()
        server.start()
    }

    @After
    open fun tearDown() {
        server.shutdown()
    }

    @Throws(IOException::class)
    fun enqueueMockResponse(responseCode : RESPONSECODES = OK, body : String = "{}")  {
        val response = MockResponse()
        response.setResponseCode(responseCode.code)
        response.setBody(body)
        server.enqueue(response)
    }

    @Throws(IOException::class)
    fun enqueueMockResponse(filename : String){
        val body : String = FileExtensions.bodyFromFile(javaClass, filename)
        enqueueMockResponse(body = body)
    }

    @Throws(IOException::class)
    fun enqueueTimeoutResponse(responseCode: RESPONSECODES = OK, body : String = "{}"){
        val response = MockResponse()
        response.setResponseCode(responseCode.code)
        response.setBody(body)
        response.throttleBody(1, 1, TimeUnit.SECONDS)
        server.enqueue(response)
    }

    @Throws(IOException::class)
    fun enqueueTimeoutResponse(filename: String){
        val body : String = FileExtensions.bodyFromFile(javaClass, filename)
        enqueueTimeoutResponse(body = body)
    }


    fun givenApiClient(): ApiRest {

        val client = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(server.url("/").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(schedulerRule.testScheduler))
                .client(client)
                .build()

        return retrofit.create(ApiRest::class.java)
    }

    fun assertSuperHeroesResponse(data : SuperHeroes){
        assertThat(data, notNullValue())
        assertThat(data.superheroes, hasSize(6))
        assertFirstEntry(data.superheroes[0])
    }

    fun assertFirstEntry(heroe: Heroe) {
        assertThat(heroe.name, `is`("Captain Marvel"))
        assertThat(heroe.photo, `is`("https://i.annihil.us/u/prod/marvel/i/mg/c/10/537ba5ff07aa4/standard_xlarge.jpg"))
        assertThat(heroe.realName, `is`("Carol Danvers"))
        assertThat(heroe.height, `is`("1.80m"))
        assertThat(heroe.abilities, `is`("Ms. Marvel is a skilled pilot & hand to hand combatant"))
        assertThat(heroe.power, `is`("Ms. Marvel's current powers include flight, enhanced strength, durability and the ability to shoot concussive energy bursts from her hands."))
        assertThat(heroe.groups, `is`("Avengers, formerly Queen's Vengeance, Starjammers"))

    }
}