package com.eeema.superheroesproject.datasource.utils

import io.reactivex.functions.Consumer
import io.reactivex.functions.Predicate

/**
 * Created by 0013440 on 22/06/2017.
 */
object PredicateUtils {
    fun <T> check(consumer: Consumer<T>): Predicate<T> {
        return Predicate { t ->
            consumer.accept(t)
            true
        }
    }
}
