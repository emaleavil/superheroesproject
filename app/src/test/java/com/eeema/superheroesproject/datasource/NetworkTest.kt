
package com.eeema.superheroesproject.datasource

import com.eeema.superheroesproject.ApiRestTest
import com.eeema.superheroesproject.datasource.utils.PredicateUtils
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.functions.Consumer
import org.junit.Before
import org.junit.Test
import java.net.SocketTimeoutException

class NetworkTest : ApiRestTest() {

    lateinit var net: Network

    @Before
    override fun setUp() {
        super.setUp()
        net = Network(givenApiClient())
    }


    @Test
    fun should_return_not_found_exception() {
        enqueueMockResponse(responseCode = RESPONSECODES.NOT_FOUND)

        val testObserver = net.listOfHeroes().test()

        schedulerRule.testScheduler.triggerActions()
        testObserver.awaitTerminalEvent()

        testObserver
                .assertError(HttpException::class.java)
    }


    @Test
    fun should_return_server_refused_exception_if_server_not_respond() {
        enqueueMockResponse(responseCode = RESPONSECODES.SERVER_DOWN)
        val testObserver = net.listOfHeroes().test()

        schedulerRule.testScheduler.triggerActions()
        testObserver.awaitTerminalEvent()

        testObserver
                .assertError(HttpException::class.java)
    }

    @Test
    fun should_parse_successfully() {
        enqueueMockResponse("list_of_heroes.json")
        val testObserver = net.listOfHeroes().test()

        schedulerRule.testScheduler.triggerActions()
        testObserver.awaitTerminalEvent()

        testObserver
                .assertNoErrors()
                .assertValue(PredicateUtils.check(Consumer { superheroes -> assertSuperHeroesResponse(superheroes) }))
    }

    @Test
    fun should_thrown_timeout_exception_if_request_takes_so_much_time() {
        enqueueTimeoutResponse("list_of_heroes.json")
        val testObserver = net.listOfHeroes().test()


        schedulerRule.testScheduler.triggerActions()
        testObserver.awaitTerminalEvent()

        testObserver
                .assertError(SocketTimeoutException::class.java)
    }

}