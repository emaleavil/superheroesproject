package com.eeema.superheroesproject.datasource.exceptions

/**
 * Created by 0013440 on 21/04/2017.
 */
class ServerRefusedException : Exception()
class UnknownException : Exception()
class SimunTimeoutException : Exception()
class NotFoundResourceException : Exception()