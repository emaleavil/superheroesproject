package com.eeema.superheroesproject.datasource.local

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.Database
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table


/**
 * Created by 0013440 on 22/06/2017.
 */
@Database(name = SuperHeroesDatabase.companion.NAME, version = SuperHeroesDatabase.companion.VERSION)
class SuperHeroesDatabase{

    object companion{
        const val NAME = "superheroes"
        const val VERSION = 1
    }
}

@Table(database = SuperHeroesDatabase::class)
data class SuperHeroesEntity(@PrimaryKey(autoincrement = true) val id : Int = 0, @Column var name : String, @Column var photo : String,
                             @Column var realName : String, @Column var height : String, @Column var power : String, @Column var abilities : String,
                             @Column var groups : String)

