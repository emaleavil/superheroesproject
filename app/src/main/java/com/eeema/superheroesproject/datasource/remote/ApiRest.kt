package com.eeema.superheroesproject.datasource.remote

import com.eeema.superheroesproject.model.SuperHeroes
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by 0013440 on 20/06/2017.
 */
interface ApiRest {

    @GET("bvyob")
    fun listOfHeroes() : Single<SuperHeroes>
}