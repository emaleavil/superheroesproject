package com.eeema.superheroesproject.datasource

import com.eeema.superheroesproject.model.SuperHeroes
import io.reactivex.Observable

/**
 * Created by 0013440 on 22/06/2017.
 */
interface DataSource {

    fun retrieveHeroes() : Observable<SuperHeroes>

    fun saveHeroes(heroes : SuperHeroes) : Unit

    fun heroe(name : String)

}