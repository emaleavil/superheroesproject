package com.eeema.superheroesproject.datasource.local

import android.content.Context
import com.eeema.superheroesproject.datasource.DataSource
import com.eeema.superheroesproject.model.SuperHeroes
import com.eeema.superheroesproject.utils.SchedulerProvider
import com.squareup.sqlbrite.BriteDatabase
import io.reactivex.Observable
import com.squareup.sqlbrite.SqlBrite



/**
 * Created by 0013440 on 22/06/2017.
 */
class LocalDataSource(context: Context, provider : SchedulerProvider) : DataSource {

    lateinit var briteDB : BriteDatabase


    init{
        val dbHelper = SuperHeroesDBHelper(context)
        val sqlBrite = SqlBrite.create()
        briteDB = sqlBrite.wrapDatabaseHelper(dbHelper, provider.io())
    }

    override fun saveHeroes(heroes: SuperHeroes) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun heroe(name: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun retrieveHeroes(): Observable<SuperHeroes> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}