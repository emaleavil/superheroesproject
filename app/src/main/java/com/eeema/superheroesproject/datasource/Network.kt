package com.eeema.superheroesproject.datasource

import com.eeema.superheroesproject.datasource.remote.ApiRest
import com.eeema.superheroesproject.model.SuperHeroes
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by 0013440 on 20/06/2017.
 */
class Network(val api : ApiRest) {

    val TIMEOUT : Long = 20

    fun listOfHeroes() : Single<SuperHeroes> {
        return api
            .listOfHeroes()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .timeout(TIMEOUT,TimeUnit.SECONDS)

    }

}