package com.eeema.superheroesproject.model

/**
 * Created by 0013440 on 20/06/2017.
 */

data class SuperHeroes(val superheroes : List<Heroe> = arrayListOf())

data class Heroe(val name : String, val photo : String, val realName : String, val height : String, val power : String, val abilities : String, val groups : String)