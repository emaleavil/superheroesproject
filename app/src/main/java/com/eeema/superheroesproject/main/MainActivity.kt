package com.eeema.superheroesproject.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.eeema.superheroesproject.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
