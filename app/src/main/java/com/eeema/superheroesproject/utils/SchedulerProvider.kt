package com.eeema.superheroesproject.utils

import rx.Scheduler


/**
 * Created by 0013440 on 22/06/2017.
 */
interface SchedulerProvider {

    fun io() : Scheduler
    fun mainThread() : Scheduler
}