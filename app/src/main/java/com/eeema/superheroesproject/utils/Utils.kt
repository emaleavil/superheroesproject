package com.eeema.superheroesproject.utils
import java.io.*

object Strings {
    @Throws(IOException::class)
    fun generateStringResponse(`is`: InputStream): String {

        val builder = StringBuilder()

        val reader = BufferedReader(InputStreamReader(`is`))

        var line: String? = null
        while ({line = reader.readLine(); line }() != null) {
            builder.append(line)
        }

        val jsonObject = builder.toString()
        reader.close()

        return jsonObject
    }
}


object FileExtensions{

    fun bodyFromFile(clazz : Class<Any>, filePath : String) : String {
        val loader : ClassLoader = clazz.classLoader
        val file : File = File(loader.getResource(filePath).file)
        return org.apache.commons.io.FileUtils.readFileToString(file, "UTF-8")
    }
}